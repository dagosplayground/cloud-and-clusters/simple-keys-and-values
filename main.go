package main

import (
	"fmt"
	"github.com/gofiber/fiber/v2"
	"os"
)

const (
	DATAFILE = "./dataFile.gob"
)

var (
	app  *fiber.App
	DATA map[string]interface{}
)

func init() {
	app = fiber.New()
	errData := load()
	if errData != nil {
		fmt.Println(errData)
		save()
	}
}

func GetRoot(c *fiber.Ctx) error {
	return c.JSON(DATA)
}

/*
PostRoot
Takes a JSON post request formated such as:

	{
	    "uniqueKey": {
	        "Name": "nameOfValue",
	        "Value": "someValue"
	    }
	}
*/
func PostRoot(c *fiber.Ctx) error {
	var payload map[string]interface{}

	if err := c.BodyParser(&payload); err != nil {
		return err
	}

	/* Get Keys */
	k := make([]string, len(payload)) // Keys slice
	i := 0                            // iteration counter

	// copy payload keys into k
	for s, _ := range payload {
		k[i] = s
		i++
	}

	for _, kname := range k {
		if !ADD(kname, payload[kname].(map[string]interface{})) { // Add the data to the keyvalue store
			fmt.Println("Add operation failed!")
			c.SendStatus(404)
		} else {
			errSave := save()
			if errSave != nil {
				fmt.Println("Save after ADD failed")
				fmt.Println(errSave)
			}
		}
	}
	return c.JSON(payload)
}

/*
Delete item if found in the key value store
*/
func DelRoot(c *fiber.Ctx) error {
	var payload string

	if err := c.BodyParser(&payload); err != nil {
		return err
	}

	fmt.Println("Input Delete: ", payload)

	if !DELETE(payload) { // Delete the data to the keyvalue store
		fmt.Println("Delete operation failed!")
	} else {
		errSave := save()
		if errSave != nil {
			fmt.Println("Save after delete failed")
			fmt.Println(errSave)
		}
	}

	return c.JSON(payload)
}

// Updates current entry
func UpdateRoot(c *fiber.Ctx) error {
	var payload map[string]interface{}

	if err := c.BodyParser(&payload); err != nil {
		return err
	}

	/* Get Keys */
	k := make([]string, len(payload)) // Keys slice
	i := 0

	//  copy payload keys into k
	for s, _ := range payload {
		k[i] = s
		i++
	}

	for _, kname := range k {
		if !CHANGE(kname, payload[kname].(map[string]interface{})) { // Add the data to the keyvalue store
			fmt.Println("Add operation failed!")
		} else {
			errSave := save()
			if errSave != nil {
				fmt.Println("Save after ADD failed")
				fmt.Println(errSave)
			}
		}
	}
	return c.JSON(payload)
}

/*
Lookup Entry (single and explicit)
Curl Get on /get with data with the string of the key name.
*/
func Lookup(c *fiber.Ctx) error {

	lookupVal := string(c.Body())

	retval := LOOKUP(lookupVal)

	if retval == nil {
		c.SendString("Key name of " + lookupVal + " not found.")
		return c.SendStatus(404)
	} else {
		return c.JSON(retval)
	}
}

func main() {
	PORT := ":8900"
	arguments := os.Args
	if len(arguments) == 2 {
		PORT = ":" + arguments[1]
	}

	fmt.Printf("Using port number: %s\n", PORT)

	app.Get("/", GetRoot).Name("home")
	app.Post("/", PostRoot).Name("write")
	app.Delete("/", DelRoot).Name("remove")
	app.Put("/", UpdateRoot).Name("update")
	app.Get("/get", Lookup).Name("get")

	app.Listen(PORT)
}
