[[_TOC_]]

# API Server

Basic API server used for mock up of an API endpoint for Terraform to interact with for end to end testing. 

*Note:* May need to launch twice to create the gob file to store the data on the backend.

# Build

To build, the classic steps for building. 
```bash
go mod init main.go
go mod tidy
go build -o api
```

# Use

All the test cases assume the use of `curl` either in Git Bash or natively on the system. The `testapi.sh` script is used for local testing to verify the API server is functioning as intended.

## List

Either in a browser or through curl, go to the URL. The location when the server is running with the default port is [http://localhost:8900/](http://localhost:8900/). 

The curl syntax: 
```bash
curl -i \
    -X GET \
    -H "Content-Type: application/json" \
    http://localhost:8900/
```

## Add

Writing to the API is routed to the root level, but make sure to set the action as a post. 

```bash
curl -i \
    -X POST \
    -H "Content-Type: application/json" \
    --data '{"localtest":{"Name": "TestData", "Value": "12345Blah"}}' \
    http://localhost:8900/
```

## Delete

Note the simplified syntax for a JSON application type. The delete statement is only designed to take a single key at a time. 

```bash
curl -i \
    -X DELETE \
    -H "Content-Type: application/json" \
    --data '"localtest"' \
    http://localhost:8900/
```

## Update

For updating, the post `PUT` method is used. Note, this will fail if the item mentioned does not exist already. 

```bash
curl -i \
    -X PUT \
    -H "Content-Type: application/json" \
    --data '{"localtest":{"Name": "TestData", "Value": "12345Blah"}}' \
    http://localhost:8900/
```